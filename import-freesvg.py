#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2018 Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
FreeSVG Importer
"""

import os
import logging

# This is needed because Inkscape doesn't control python paths correctly.
#import inkpath
#inkpath.load_prefixes()

from inkman.gtkme import GtkApp, Window, PixmapManager, TreeView, asyncme

DATA_DIR = os.path.join(os.path.dirname(__file__), 'gui')

class SearchTreeView(TreeView):
    """A Search results list"""
    def setup(self, *args, **kwargs):
        """Setup the treeview with one or many columns manually"""
        pixmaps = self.args['pixmaps']
  
        def get_name(item):
            return "<big><b>{}</b></big>\n<i>{}</i>".format(item.name, item.summary)
  
        col = self.ViewColumn('Search Result', expand=True,\
            text=get_name, template=None,\
            icon=lambda item: pixmaps.get(item.get_icon()),\
            pad=0, size=None)
  
        col._icon_renderer.set_property('ypad', 2) # pylint: disable=protected-access
        col._text_renderer.set_property('xpad', 8) # pylint: disable=protected-access
  
        self.ViewColumn('Version', expand=False,\
            text=lambda item: item.version, template="<i>{}</i>", pad=6)
  
        self.ViewColumn('Author', expand=False,\
            text=lambda item: item.author, pad=6)
  
        self.ViewSort(data=lambda item: item.name, ascending=True, contains='name')


class ImportFreeSvgWindow(Window):
    name = 'import-freesvg'

    def __init__(self, *args, **kwargs):
        Window.__init__(self, *args, **kwargs)
        self.pixmaps = PixmapManager('pixmaps', pixmap_dir=DATA_DIR, size=48, load_size=(96,96))

        self.searching = self.widget('dl-searching')
        self.searchbox = self.searching.get_parent()
        self.searchbox.remove(self.searching)

        self.results = SearchTreeView(
            self.widget('search_results'),
            pixmaps=self.pixmaps,
            selected=self.select_local)

        self.refresh_local()
        self.window.show_all()
        #self.widget('local_install').set_sensitive(bool(self.install_to))

    def change_local_all(self, widget, unk):
        """When the source switch button is clicked"""
        self.refresh_local()

    def refresh_local(self):
        """Searches for locally installed extensions and adds them"""
        pass

    def select_local(self, item):
        """Select an installed extension"""
        #self.widget('local_uninstall').set_sensitive(item.is_writable())

    def local_uninstall(self, widget):
        """Uninstall selected extection package"""
        item = self.local.selected
        if item.is_writable():
            if item.uninstall():
                self.local.remove_item(item)
                # In the even it happened to be in a remote search at the same time
                for search_item, treeiter in self.remote:
                    if item.name == search_item.name:
                        search_item.installed = None
                self.remote.refresh()

    def change_remote_all(self, widget, unk):
        """When the source switch button is clicked"""
        self.remote_search(self.widget('dl-search'))

    def remote_search(self, widget):
        """Remote search activation"""
        filtered = not self.widget('remote_all').get_active()
        query = widget.get_text()
        if len(query) > 2:
            print("searching for: '{}'".format(query))
            self.remote.clear()
            self.widget('remote_install').set_sensitive(False)
            self.widget('dl-search').set_sensitive(False)
            self.searchbox.add(self.searching)
            self.widget('dl-searching').start()
            self.async_search(query, filtered)

    @asyncme.run_or_none
    def async_search(self, query, filtered):
        """Asyncronous searching in PyPI"""
        for package in self.archive.search(query, filtered):
            self.add_search_result(package)
        self.search_finished()

    @asyncme.mainloop_only
    def add_search_result(self, package):
        """Adding things to Gtk must be done in mainloop"""
        self.remote.add_item([package])

    @asyncme.mainloop_only
    def search_finished(self):
        """After everything, finish the search"""
        self.searchbox.remove(self.searching)
        self.widget('dl-search').set_sensitive(True)
        self.replace(self.searching, self.remote)

    def select_remote(self, item):
        """Select an installed extension"""
        # Do we have a place to install packages to?
        active = self.install_to \
            and self.remote.selected \
            and not self.remote.selected.installed
        #self.widget('remote_install').set_sensitive(active)

    def remote_install(self, widget):
        """Install a remote package"""
        #self.widget('remote_install').set_sensitive(False)
        item = self.remote.selected
        item.install_to(self.install_to.path)
        self.refresh_local()
        self.remote.refresh()

    def local_install(self, widget):
        """Install from a local filename"""
        pass

    def install_to(self):
        pass

class ImportApp(GtkApp):
    """Load the free-svg importer glade file and attach to window"""
    glade_dir = os.path.join(os.path.dirname(__file__), 'gui')
    app_name = 'import-freesvg'
    windows = [ImportFreeSvgWindow]

if __name__ == '__main__':
    try:
        ImportApp(start_loop=True)
    except KeyboardInterrupt:
        logging.info("User Interputed")
    logging.debug("Exiting Application")
