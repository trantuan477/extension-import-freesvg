#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2018-2019 Martin Owens
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
"""
Setup for the FreeSVG importer.
"""

from setuptools import setup
from inkman.utils import get_description

setup(
    name='inkscape-import-freesvg',
    version='0.6',
    description='Import FreeSVG',
    long_description=get_description(__file__),
    author='Martin Owens',
    url='https://gitlab.com/inkscape/import-freesvg',
    author_email='doctormo@gmail.com',
    platforms='linux',
    license='GPLv3',
    include_package_data=True,
    #packages=[],
    data_files=[
        ('inx', ['import-freesvg.inx']),
    ],
    scripts=['import-freesvg.py'],
    classifiers=[
        'Environment :: Plugins',
        'Topic :: Multimedia :: Graphics :: Editors :: Vector-Based',

        'Intended Audience :: Other Audience',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',

        'Development Status :: 5 - Production/Stable',
    ],
    install_requires=['inkscape-extensions-manager'],
)
