# Import from Free SVG

This extension provides a simple user interface for searching for and important an svg from FreeSVG online service. This service requires that you have a user account which you will be linked to creating when running the extension for the first time.

# Inkscape Extensions Manager

This Gtk3 extension is designed to be installed and run via the Inkscape Extension Manager. If you're not sure if you have the extension manager installed, you will need to check 'Extension Manager' in your extensions menu and install it first ifit's missing.

